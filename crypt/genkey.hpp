/*
CRYXOR 2013 EST SOUS LICENCE GNU GPL
Programme de chiffrement et de dechiffrement symetrique -Masque Jetable-
Sopheny (Cassiopeeu) 2013
Le programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifi
er au titre des clauses de la Licence Publique Générale GNU, telle que publiée p
ar la Free Software Foundation ; soit la version 2 de la Licence, ou (à votre di
scrétion) une version ultérieure quelconque. Ce programme est distribué dans l'e
spoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même une garantie impli
cite de COMMERCIABILITE ou DE CONFORMITE A UNE UTILISATION PARTICULIERE. Voir la
icence Publique Générale GNU pour plus de détails. Vous devriez avoir reçu un ex
emplaire de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas
le cas, écrivez à la Free Software Foundation Inc.,  51 Franklin Street, Fifth F
loor, Boston, MA 02110-1301, USA.
*/

#ifndef GENKEY_HPP
#define GENKEY_HPP
#include <QCoreApplication>
#include <QFile>
#include <QString>
#include <QDataStream>

class Genkey
{
public:
    Genkey();
    void setfile(QString path);
    void setsize(quint64 size);
    void generate();
private:
    QString m_path_file;
    quint64 m_size;
};

#endif // GENKEY_HPP
