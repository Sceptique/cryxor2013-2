#include "configreader.hpp"

#include <iostream>
using namespace std;
ConfigReader::ConfigReader()
{
}

QString ConfigReader::findParameter(QString parameter)
{
    QFile config_file(CONF_FILE_PATH);
    config_file.open(QIODevice::ReadOnly | QIODevice::Text);
    QStringList config_data_line;
    QString config_data(config_file.readAll());
    int i(-1);

    config_data_line = config_data.split("\n");
    for (int c=0;c<config_data_line.size();c++)
    {
        if (config_data_line[c].left(parameter.size()+1) == (parameter+":"))
            i = c;
    }
    //cout << config_data_line[i].right(config_data_line[i].size()-parameter.size()-1).toUtf8().data() << endl;
    return (config_data_line[i].right(config_data_line[i].size()-parameter.size()-1));
}
