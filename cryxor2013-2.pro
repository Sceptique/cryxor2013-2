#-------------------------------------------------
#
# Project created by QtCreator 2013-03-05T21:25:06
#
#-------------------------------------------------


#QT       += core gui
#QT       += widgets
#QT       += gui declarative
#CONFIG   += qt gui
CONFIG   += qt

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cryxor2013_2
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    configreader.cpp \
    mypassword_windows.cpp \
    crypt/cryxor.cpp \
    crypt/genkey.cpp

HEADERS  += \
    mainwindow.hpp \
    configreader.hpp \
    mypassword_windows.hpp \
    crypt/genkey.hpp \
    crypt/cryxor.hpp

FORMS    +=

OTHER_FILES += \
    README.md \
    manuel.html \
    cryxor2013.conf \
# use it on windows to compile with meta infos and icon
    #cryxor2013_2.rc
    NOTICE.md

RESOURCES += \
    images.qrc

#the icon on linux
#ICON = cryxor2013.svg
