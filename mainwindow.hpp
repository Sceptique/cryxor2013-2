/*
CRYXOR 2013 EST SOUS LICENCE GNU GPL
Programme de chiffrement et de dechiffrement symetrique -Masque Jetable-
Sopheny (Cassiopeeu) 2013
Le programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifi
er au titre des clauses de la Licence Publique Générale GNU, telle que publiée p
ar la Free Software Foundation ; soit la version 2 de la Licence, ou (à votre di
scrétion) une version ultérieure quelconque. Ce programme est distribué dans l'e
spoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même une garantie impli
cite de COMMERCIABILITE ou DE CONFORMITE A UNE UTILISATION PARTICULIERE. Voir la
icence Publique Générale GNU pour plus de détails. Vous devriez avoir reçu un ex
emplaire de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas
le cas, écrivez à la Free Software Foundation Inc.,  51 Franklin Street, Fifth F
loor, Boston, MA 02110-1301, USA.
*/

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP
#include <QCoreApplication>
#include <QGuiApplication>
#include <QApplication>
#include <QWidget>
#include <QClipboard>
#include <QMessageBox>
#include <QCheckBox>
#include <QDesktopServices>
#include <QLayout>
#include <QFileDialog>
#include <QStatusBar>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QString>
#include <QUrl>
#include <QFile>
#include <QFileInfo>

#include "crypt/genkey.hpp"
#include "crypt/cryxor.hpp"
#include "mypassword_windows.hpp"

class MainWindow : public QWidget
{
    Q_OBJECT

    public:
        MainWindow(char* version);

    private:
        void initializeAllWidgets(QString version);
        void initializeAllLayouts();
        void initializeAllSignals();
        bool testIFile();
        bool testKFile();
        bool testOFile();
        void fileRemove(QString path);
        quint64 fileSize(QString path);
        void generateRandomFile(QString path, quint64 size);
        void execKMode();

    public slots:
        void switchKMode();
        void copyKLine();
        void pasteKLine();
        void selectIFile();
        void selectKFile();
        void selectOFile();
        QString saveFile();
        void openGuidTXT();
        void openGuidWeb();
        void openAPropos();
        void fileSafeRemove();
        void testBeforeEncrypt();
        void encrypt();
        void generateNewKey();

    signals:

    private:
        MyPassword_Windows *m_mypassword_windows;
        //MENU
        QMenuBar *m_menuBar;
        QMenu *m_menuFile;
        QMenu *m_menuOption;
        QMenu *m_menuAbout;
        QAction *m_menuFile_actionQuit;
        QAction *m_menuOption_actionMyPassword;
        QAction *m_menuOption_actionSafeRemoveFile;
        QAction *m_menuAbout_actionGuidTXT;
        QAction *m_menuAbout_actionGuidWeb;
        QAction *m_menuAbout_actionAPropos;
        QStatusBar *m_statusMainbar;

        // MAIN WIDGET
        QVBoxLayout *m_layoutMain;

        // MAIN
        QGridLayout *m_layoutFiles;
        //QGridLayout *m_glayoutOptions; // RISK -> overload?
        QPushButton *m_buttonRun;
        QPushButton *m_buttonQuit;

        // FILES
        QLabel *m_labelIFile;
        QLabel *m_labelKFile;
        QLabel *m_labelOFile;

        QLineEdit *m_lineIFile; // contient le nom des fichiers
        QLineEdit *m_lineKFile;
        QLineEdit *m_lineOFile;

        QCheckBox *m_boxIDeletion;
        QLabel *m_labelIDeletion;

        QPushButton *m_buttonIFile; // ouvre les boites de dialogue
        QPushButton *m_buttonKFile;
        QPushButton *m_buttonOFile;

        QPushButton *m_buttonKMode; // permet de choisir de mode de la clé
        QPushButton *m_buttonKCopy; // permet de copier la clé
        QPushButton *m_buttonKPaste; // de coller la clé
        QPushButton *m_buttonKGenerate; // de coller la clé

        int m_KMode;
};

#endif // MAINWINDOW_HPP
