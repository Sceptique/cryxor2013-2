/*
CRYXOR 2013 EST SOUS LICENCE GNU GPL
Programme de chiffrement et de dechiffrement symetrique -Masque Jetable-
Sopheny (Cassiopeeu) 2013
Le programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifi
er au titre des clauses de la Licence Publique Générale GNU, telle que publiée p
ar la Free Software Foundation ; soit la version 2 de la Licence, ou (à votre di
scrétion) une version ultérieure quelconque. Ce programme est distribué dans l'e
spoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même une garantie impli
cite de COMMERCIABILITE ou DE CONFORMITE A UNE UTILISATION PARTICULIERE. Voir la
icence Publique Générale GNU pour plus de détails. Vous devriez avoir reçu un ex
emplaire de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas
le cas, écrivez à la Free Software Foundation Inc.,  51 Franklin Street, Fifth F
loor, Boston, MA 02110-1301, USA.
*/

#define VERSION             "2.2.10-alpha"
#define CRYXOR_VERSION      "1.1-release"
#define GENKEY_VERSION      "1.0-release"
#define MYPASSWORD_VERSION  "0.1.4-alpha"

#include <QGuiApplication>
#include <QApplication>
#include <iostream>
#include <string>
#include "mainwindow.hpp"

using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    if (argc == 1)
    {
        MainWindow w(VERSION);
        w.show();
        return (a.exec());
    }
    else
    {
        QString ifile;
        QString kfile;
        QString ofile;
        bool kmanual = false;
        Cryxor encrypter(1);
        cout << "Lancement de Cryxor en mode console." << endl;
        if (argc >= 4)
        {
            ifile = argv[1];
            kfile = argv[2];
            ofile = argv[3];
            if (argc >= 5 && (argv[4] == "1" || argv[4] == "true"))
            {
                kmanual = true;
            }
        }
        else
        {
            string tmp;
            cout << "Vous n'avez pas indiqué assez d'arguments. Passage en mode manuel." << endl;
            cout << "Fichier à chiffrer/déchiffrer > ";
            cin >> tmp;
            ifile = QString::fromStdString(tmp);
            cout << "Votre clef est-elle dans un fichier ? (oui/non) > ";
            cin >> tmp;
            if (tmp == "oui" )//|| tmp == "yes" || tmp = "1")
            {
                kmanual = true;
            }
            if (kmanual)
            {
                cout << "Votre clef brute > ";
                cin >> tmp;
            }
            else
            {
                cout << "Fichier contenant la clef > ";
                cin >> tmp;
            }
             QString::fromStdString(tmp);
            cout << "Fichier de sortie (sauvegarde) > ";
            cin >> tmp;
            ofile = QString::fromStdString(tmp);
        }
        cout << "Les données ont étées saisies correctement." << endl;
        encrypter.setifile(ifile);
        encrypter.setkfile(kfile);
        encrypter.setofile(ofile);
        encrypter.setmode(kmanual);
        cout << "Chiffrement en cours... ";
        encrypter.encryptQ();
        cout << "Chiffrement terminé !" << endl;
        return (0);
    }
    return (0xffff);
}
